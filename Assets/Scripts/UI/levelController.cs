﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levelController : MonoBehaviour {
    public int levelNumber; // number of level (scene)
    //public static int Score = 0; // score of this level
    public static int[] Scores = new int[3]; // scores of all levels
    public Image levelNumberIcon; // icon of level number
    public Image[] loons = new Image[3]; // upper loons
    public Sprite loonIconOff; // upper loon OFF icon
    public Sprite loonIconOn; // upper loon ON icon
    public Sprite numberIconOff; // RED icon of level number
    public Sprite numberIconOn; // BLUE icon of level number
    void Awake() {
        if(PlayerPrefs.HasKey("valueLevelScore_" + levelNumber)) { // if data saved
            Scores[levelNumber-2] = PlayerPrefs.GetInt("valueLevelScore_" + levelNumber); // load saved data
        }
    }
    void Start() {
        if(scoreUpdate.openedLevels >= levelNumber) { // when level will open set ON
            levelNumberIcon.GetComponent<Image>().sprite = numberIconOn;
            GetComponent<Button>().interactable = true;

            for(int i = 0; i < 3; i++) { // set loonOFF to loons when level has opened
                loons[i].GetComponent<Image>().sprite = loonIconOff;
            }
            
            if(levelNumber == currentGameSituation.activeScene) { // set loonON when level has completed or repeated
                if(CakieController.countLoon >= Scores[levelNumber-2] && currentGameSituation.finish == true) { // if new score > old score
                    scoreUpdate.totalLoonsCount -= Scores[levelNumber-2]; // set new total loons count
                    Scores[levelNumber-2] = 0;
                    Scores[levelNumber-2] += CakieController.countLoon;
                    scoreUpdate.totalLoonsCount += Scores[levelNumber-2]; // set new total loons count

                    PlayerPrefs.SetInt("valueTotalCountLoons", scoreUpdate.totalLoonsCount); // save data
                }
            }

            PlayerPrefs.SetInt("valueLevelScore_" + levelNumber, Scores[levelNumber-2]); // save data

            for(int j = 0; j < Scores[levelNumber-2]; j++) { // change upper loons to ON
                loons[j].GetComponent<Image>().sprite = loonIconOn;
            }
        } else { // stay OFF if level hasnt opened
            levelNumberIcon.GetComponent<Image>().sprite = numberIconOff;
            GetComponent<Button>().interactable = false;
        }
    }
}
