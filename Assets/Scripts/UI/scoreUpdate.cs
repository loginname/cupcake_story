﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreUpdate : MonoBehaviour {
    public static int openedLevels = 2; // value of opened levels
    public static int totalLoonsCount = 0; // total saved loons from all levels (story mode)
    public Text totalLoonsCountText; // saved loons from all levels
    public static float bestScore = 0; // best distance of all time(endless mode)
    public static int burstLoons = 0; // value of all burst loons from all levels
    public Text bestScoreText;
    public Text burstLoonsText;
    void Awake() {
        if(PlayerPrefs.HasKey("valueOpenedLevels")) // if register has key
            openedLevels = PlayerPrefs.GetInt("valueOpenedLevels"); // if data saved
        if(PlayerPrefs.HasKey("valueBurstLoons")) // if register has key
            burstLoons = PlayerPrefs.GetInt("valueBurstLoons"); // if data saved
        if(PlayerPrefs.HasKey("valueBestScore")) // if register has key
            bestScore = PlayerPrefs.GetFloat("valueBestScore"); // if data saved
        if(PlayerPrefs.HasKey("valueTotalCountLoons")) // if register has key
            totalLoonsCount = PlayerPrefs.GetInt("valueTotalCountLoons"); // if data saved
    }
    void Update() {
        bestScoreText.text = bestScore.ToString("0.0");
        burstLoonsText.text = burstLoons.ToString();
        totalLoonsCountText.text =  totalLoonsCount.ToString();
    }
}
