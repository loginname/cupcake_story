﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxoonController : MonoBehaviour {
    public GameObject cardBoardBox; // box on the loons
    public Joystick joyStick; // element of movement
    public static GameObject[] setLoons = new GameObject[3]; // loons
    public Transform[] rotationsLoons = new Transform[3]; // start loons'es rotations
    public Transform rotationBox; // start box'es rotation
    public static float idleSpeed = 1; // speed of the idle
    public static float moveSpeed = 3; // how fast box move via joystick
    void Update() {
        //boxoon_position = GetComponent<Transform>();

        // minY = Camera.main.GetComponent<Transform>().position.y - 5.5f;
        // maxY = Camera.main.GetComponent<Transform>().position.y + 4f;
        // Vector2 curPos = transform.position;
        // curPos.y = Mathf.Clamp(transform.position.y, minY, maxY);
        // curPos.x = Mathf.Clamp(transform.position.x, minX, maxX);
        // transform.position = curPos;
        
        if (setLoons[0].CompareTag("loon") || setLoons[1].CompareTag("loon") || setLoons[2].CompareTag("loon")) { // if any loon if exist
            if(joyStick.Vertical >= .2f && transform.position.y < Camera.main.transform.position.y + 4) { // Up
                transform.Translate(Vector2.up * Time.deltaTime * moveSpeed);
            } else if(joyStick.Vertical <= -.2f && transform.position.y > Camera.main.transform.position.y - 5.5) { // Down
                transform.Translate(Vector2.down * Time.deltaTime * moveSpeed);
            } else { // Idle
                transform.Translate(Vector2.up * Time.deltaTime * idleSpeed);
            }
            
            if(joyStick.Horizontal >= .2f && transform.position.x < 3) { // Right
                transform.Translate(Vector2.right * Time.deltaTime * moveSpeed);
                if(setLoons[2].transform.localEulerAngles.z > 290) { // if right loon isnt at the edge
                    for(int i = 0; i < setLoons.Length; i++) { // rotate loons to right side
                        setLoons[i].transform.Rotate(0, 0, -Time.deltaTime * 100);
                    }   
                    cardBoardBox.transform.Rotate(0, 0, -Time.deltaTime * 100); // rotate box to right side
                }
            } else if(joyStick.Horizontal <= -.2f && transform.position.x > -3) { // Left
                transform.Translate(Vector2.left * Time.deltaTime * moveSpeed);
                if(setLoons[0].transform.localEulerAngles.z < 70) { // if left loon isnt at the edge
                    for(int i = 0; i < setLoons.Length; i++) {  // rotate loons to left side
                        setLoons[i].transform.Rotate(0, 0, Time.deltaTime * 100);
                    }
                    cardBoardBox.transform.Rotate(0, 0, Time.deltaTime * 100); // rotate box to left side
                }
            } else { // return to start position
                for(int i = 0; i < setLoons.Length; i++) {
                    if(setLoons[i].transform != rotationsLoons[i]) { // if loon isnt on the start rotation
                        setLoons[i].transform.rotation = Quaternion.Slerp(setLoons[i].transform.rotation, rotationsLoons[i].rotation, Time.deltaTime * 10);
                    }
                }
                cardBoardBox.transform.rotation = Quaternion.Slerp(cardBoardBox.transform.rotation, rotationBox.rotation, Time.deltaTime * 10);
            }
        } else { // game over
            transform.Translate(Vector2.down * Time.deltaTime * 3); // fall down
            //Destroy(gameObject, 5); // destroy the boxoon
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // if(Input.GetKey(KeyCode.A) && boxoon.GetComponent<Transform>().position.x > -2.5) {
            //     boxoon.transform.Translate(Vector2.left * Time.deltaTime * move_speed_horizontal);
            //     if(set_balloons[0].transform.localEulerAngles.z < 70) {
            //         for(int i = 0; i < set_balloons.Length; i++) {
            //             set_balloons[i].transform.Rotate(0, 0, Time.deltaTime * rotate_speed);
            //         }
            //         box.transform.Rotate(0, 0, Time.deltaTime * rotate_speed);
            //     }
            // } else if (Input.GetKey(KeyCode.D) && boxoon.GetComponent<Transform>().position.x < 2.5) {
            //     boxoon.transform.Translate(Vector2.right * Time.deltaTime * move_speed_horizontal);
            //     if(set_balloons[2].transform.localEulerAngles.z > 290) {
            //         for(int i = 0; i < set_balloons.Length; i++) {
            //             set_balloons[i].transform.Rotate(0, 0, -Time.deltaTime * rotate_speed);
            //         }   
            //         box.transform.Rotate(0, 0, -Time.deltaTime * rotate_speed);
            //     }
            // } else if (Input.GetKey(KeyCode.W) && boxoon.GetComponent<Transform>().position.y < camera_main.GetComponent<Transform>().position.y + 3.5) {
            //     boxoon.transform.Translate(Vector2.up * Time.deltaTime * move_speed_horizontal * move_speed_vertical);

            // } else if (Input.GetKey(KeyCode.S) && boxoon.GetComponent<Transform>().position.y > camera_main.GetComponent<Transform>().position.y - 5.5) {
            //     boxoon.transform.Translate(Vector2.down * Time.deltaTime * move_speed_horizontal * move_speed_vertical);

            // } else { // return balloons on start positions
            //     for(int i = 0; i < set_balloons.Length; i++) {
            //         if(set_balloons[i].transform != balloons_rotations[i]) {
            //             set_balloons[i].transform.rotation = Quaternion.Slerp(set_balloons[i].transform.rotation, balloons_rotations[i].rotation, return_speed * Time.deltaTime);
            //         }
            //     }
            //     box.transform.rotation = Quaternion.Slerp(box.transform.rotation, box_rotation.rotation, return_speed * Time.deltaTime);
            // }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////