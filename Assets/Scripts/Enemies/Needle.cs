﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needle : MonoBehaviour {
    private float moveSpeed = 3; // MS of needle
    public Vector2 direction; // direction to move
    void Start() {
        GetComponent<AudioSource>().Play();
    }
    void Update() {
        gameObject.transform.Translate(direction * moveSpeed * Time.deltaTime); // move
        
        Destroy(gameObject, 3);
    }
}
