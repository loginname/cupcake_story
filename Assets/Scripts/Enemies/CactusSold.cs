﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CactusSold : MonoBehaviour {
    public GameObject needle;
    public Transform positionSpawn; // position of spawn needle
    public int angle = 0; // how angle to move of needle
    public void spawnNeedle() {
        Instantiate(needle, new Vector2(positionSpawn.transform.position.x, positionSpawn.transform.position.y), Quaternion.Euler(0f, 0f, angle));
    }
}
