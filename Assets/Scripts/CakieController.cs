﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CakieController : MonoBehaviour {
    private Animator aniMator;
    public GameObject boxoon;
    public GameObject box;
    public GameObject boxoonImage;
    public static int countLoon = 0;
    void Start() {
        aniMator = GetComponent<Animator>();
    }
    void Update() {
        if(currentGameSituation.allChunksSpawned == false) { // if all chunk arent spawned then dont stop
            transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed);
        } else if(currentGameSituation.allChunksSpawned == true) { // if all chunk spawned then stop move
            if(transform.position.y >= levelChunksGenerator.lastChunk.transform.position.y + 15) {
                transform.Translate(Vector2.zero);
            } else { // go up
                transform.Translate(Vector2.up * Time.deltaTime * boxoonController.idleSpeed);
            }
        }
    }
    void OnTriggerEnter2D(Collider2D other) {
        if(box) {
            for(int i = 0; i < 3; i++) { // count saved loons
                if(boxoonController.setLoons[i].CompareTag("loon")) countLoon++;
            }
            // boxoon.GetComponent<SpriteRenderer>().enabled = false;
            // boxoon.GetComponent<CircleCollider2D>().enabled = false;
            // boxoon.GetComponent<HingeJoint2D>().enabled = false;
            aniMator.SetBool("finish", true); // play animation of eating
            currentGameSituation.finish = true;

            if(GetComponent<SpriteRenderer>().enabled == false) {
                boxoonImage.SetActive(true);
            }

            Destroy(boxoon.gameObject);
        }
        //for(int starCount = 0; starCount < MonsterTrigger.count / 5; starCount++) { // Opening stars = values of score
            //gameStars[starCount].GetComponent<Image>().sprite = starIconOn;
        //}
    }
}
